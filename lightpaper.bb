[center][img]https://dearth.io/wp-content/uploads/2018/08/logo_600.png[/img]

[b][u][size=20pt]dEarth[/size][/u][/b]
[color=purple][size=14pt]A [b]massively multiplayer[/b] game where items, land ownership, crafting and skill learning are hold by [b]smart contracts[/b]. 
PvP and PvE takes place in an [b]open world[/b] sustained by a [b]decentralized economy[/b][/size][/color]

[size=16pt][b]I have a question for you…  
What would happen if big data was used to [b]help humanity[/b] ?  
What would you do if you were able to [b]impact positively the real world with a virtual world[/b] ?  
How would you [b]contribute[/b] to such a project ? [/b] [/size]

[hr]
[size=16pt][b]Info[/b][/size]
[hr]
[b][url=https://dearth.io]Official website[/url][/b]
[b][url=https://gitlab.com/l1br3/dEarth]Workflow and whitepaper[/url][/b]


[hr]
[size=16pt][b]dEarth, The dMMORPG[/b][/size]
[hr]
[size=14pt][b][color=purple]SYNOPSIS[/color][/b][/size]
Humans are not monkeys anymore. Earth is sometimes sick and [b]magic forces[/b] are unpredictable.  
[b]Evolve[/b]


[size=14pt][b][color=purple]GAMEPLAY AND FEATURES[/color][/b][/size]
[size=12pt][b]PVP[/b][/size]
[b]Competing[/b] is the funniest aspect of a game.
Fight and [b]measure your skill in the arena![/b]
Get [b]world ranked[/b] to show your friends you are the highest skilled
Who will be the [b]fighter of the century[/b] ?

[size=12pt][b]OPEN WORLD[/b][/size]
A finite sphere floating in the galaxy…
[b]Fully Editable[/b] from the core to the space border
Populated with [b]vreal PNJ, fauna and flora[/b]
Sometimes disturbed by [b]magic forces…[/b]

[size=12pt][b]SOCIAL[/b][/size]
A [b]revolutionary[/b] way to interact
We bring a [b]new way to do politics[/b]
Trade, debate, brainstorm, vote, bet…
Let’s experience [b]pure democracy[/b]

[size=12pt][b]CRAFT[/b][/size]
Evolving is about [b]engineering[/b].
Farm, [b]craft and create machines[/b] which process your resources.
Participate to the first [b]decentralized economy[/b]


[size=14pt][b][color=purple]DECENTRALIZED ECONOMY[/color][/b][/size]
[size=12pt][b]TRACEABILITY[/b][/size]
Every piece of land, every item and skill [b]belong to someone[/b]
[b]XP[/b] CryptoCurrency used to trade

[size=12pt][b]SMART CRAFTING[/b][/size]
Crafting, farming, engineering are done via [b]SmartContracts[/b]
The Decentralized Economy [b]can run outside the game[/b]

[size=12pt][b]STABILITY[/b][/size]
dEarth acts as a central bank to [b]guarantee XP stability[/b]
It manages monetary mass depending of humans mass

[size=12pt][b]VREAL MONEY[/b][/size]
[b]Invest in dEconomy[/b] with your clan
Make [b]real profit from playing[/b]


[hr]
[size=16pt][b]dEarth, The Company behind the game[/b][/size]
[hr]
[size=14pt][b][color=purple]OPEN STRATEGY[/color][/b][/size]
[size=12pt][b]OPEN DATA[/b][/size]
Personal data [b]shouldn’t be sold[/b]
It should be [b]used to understand the world[/b]
[b]Open database[/b] with public API

[size=12pt][b]OPEN FINANCE[/b][/size]
Every revenue and [b]expense are public[/b]
Salaries are [b]set by the community[/b]
Investments are openly discussed and agreed

[size=12pt][b]OPEN MANAGEMENT[/b][/size]
Mixed team of internal and external staff [b]chosen by community[/b]
Open JIRA for [b]public workflow[/b]
Open time tracking to get open Key Productivity Indicators

[size=12pt][b]OPEN GOVERNANCE[/b][/size]
[b]Shared voting right[/b] with community
[b]Shared property[/b] of real world investments
[b]Shared vision[/b] of further developments


[hr]
[size=16pt][b]dEarth, The ICO[/b][/size]
[hr]
Participating in dEarth ICO is not only [b]investing in decentralized economy[/b] but it's also giving a chance to a [b]new democracy system to born, it's investing in the world of tomorrow.[/b]

[size=12pt][b]BONUSES[/b][/size]
To [b]reward earliest investors[/b] whom take the more risks, every step will offer a [b]100% bonus[/b] on the next one: 
[i]1 XPa = 2XPb = 4XPc[/i] and so on until we release the blockchain and the [b]XP[/b] cryptocurrency.

[size=12pt][b]RATES[/b][/size]
1ETH = 464 USD (30/07)
1ETH = 4640 XPa
[b]1XPa = 0.10USD[/b] (30/07)

[size=12pt][b]FUNDING GOAL (step 1)[/b][/size]
[b]33,000 USD[/b]
330,000 XPa
71.12 ETH

[size=12pt][b]DATES[/b][/size]
30[sup]th[/sup] of July 2018
30[sup]th[/sup] of October 2018
[b]3 months[/b]

[hr]
[size=16pt][b]dEarth, The dream of a life[/b][/size]
[hr]

I want this project to be [b]taken by the community[/b], I want the highest skilled people to work on this, together. Without pre-existing team, we are free to create the DreamTeam.  

I'm a professional developer and I play video games since I can hold a joystick, I do prefer [b]open worlds[/b] and freedom than scenario and story.
I'm an [b]over creative person[/b] and this make me able to change an unsolvable problem into a [b]solution[/b], it's so rewarding!
[b]My motto is: 1 hour = 1 new idea.[/b]
I'm [b]respectful[/b], I believe in non-violent way of interacting. I strongly believe that [b]humanity stands for peace[/b] and that it's just a matter of evolving.
My main life Goal is to [b]help humanity to build a peaceful and free society[/b]. I believe that a [b]real and incorruptible democracy[/b] will give us the ability to live in sustainable peace.
Internet is a gift to humanity and [b]dEarth is a laboratory for building a new set of tools which will change the world.[/b]

[i]I'm french and I live in Thaïland, I would prefer to hide my real identity right now as I'm not sure about legality of this ICO (KYC, SEC compliant, etc...).[/i]


[hr]
[size=16pt][b]dEarth, The community[/b][/size]
[hr]
[size=10pt][b]WORKFLOW[/b][/size]
[url=https://gitlab.com/l1br3/dEarth]>>Read the full lightpaper here and access to the workflow<<[/url]

[size=10pt][b]DISCORD[/b][/size]
[url=https://discordapp.com/channels/471659163748139028]>>Join the dEarth community on dIscord<<[/url]

[size=10pt][b]MY PHONE[/b][/size]
Feel free to call me on whattsapp if you have any question or just want to check if I’m a real human. (english and french)
+66 98 970 8275 

[size=10pt][b]EMAIL[/b][/size]
Send me an email at coo@kapital.games

[size=10pt][b]WEBSITE[/b][/size]
[b][url=https://dearth.io]Visit the official website[/url][/b]
[/center]