### Modifications
##### custom css 
Appearance -> customize -> Additional CSS 
```css
/* Used to hide the header bar on mobile and tablets  */
@media screen and (max-width: 1100px) {
  #header-container {
    display:none;
  }
}

/* Used to hide menu button on desktop */
@media only screen and (min-width: 1101px) {
	#header-container.header-style-7 .dfd-top-row .columns .dfd-menu-button, .dfd-menu-button {
		display:none;
	}
} 

.big_text {
	font-size: 22px;
}
```

##### Theme modification 
Testimonials content is esc_html. We have to remove esc_html from the following line.

```php
// original file 
// wp-content/themes/ronneby/inc/vc_custom/dfd_vc_addons/modules/crum-testimonials.php

// line 657
$content_html .= esc_html( $description );
```