# dEarth 

**TL;DR**  
**dEarth is a massively multiplayer game where items, land ownership, crafting and skill learning are hold by smart contracts. PvP and PvE takes place in an open world sustained by a decentralized economy**

*Feel free to open issues for discuting about the project or asking any question.*

--------------------------

## Genese
My dream when I was a child was to make a video game. From 12 to 21 I strongly believed that it was impossible to do it because people told me it was too hard. 
At 21 I understood that people beliefs shouldn't be mine and I started to learn code to make a game.  
I always thought it would be easier to make a traditional business first (service app) to have money to invest in video game industry. 
At 29, I'm starting the only game I want to develop since ever, this is my plan for the next 10 years, I'll do it with or without the ICO's success.
My dad gave me the passion for understanding how the financial sectors works and I also worked for its company.

--------------------------
## Synopsis
`Humans` are not monkeys anymore. `Earth` is sometimes sick and magic forces are unpredictable.  
**Evolve**


--------------------------

## Terminology 
- `dEarth` refers as the game itself
- `Kapital` refers at the company whom develop and own the game
- `XP` refers at `dEarth` cryptocurrency used to buy and process smart contracts
- `earth` or `nature` refers to the authoritarive server
- `players` are the real person holding the `XP` wallet
- `humans` are the players' characters


--------------------------
## Gameplay and features

### pvp
The **Fight** release will be the first public version of dEarth as a PvP **skill** game where `humans` can fight for XP. `Earth` distribute to each human a small amount of vXP (virtual, not blockchain) and `humans` have to fight to get other `humans` vXP. They can buy items and skills to `earth` shop to increase their power and unlock new features. 

### world 
Known as the **World** update, open world with ressources to gather inside will be released.  
Goal is to achieve open world standard: infinite flat earth, many layers of coordinates, fully editable (building) and biomes. Additionaly, we will emulate wildlife with real state of fauna and flora.  
XP give `humans` the right to claim some land at a fixed rate of 1XP -> 100 surface blocks.
You can find dungeons, kill bosses and gain loot and XP. 

### Trade 
The **Deal** update will add `humans` market places to the game all around the `earth` world.

### Tools
The **Make** update will bring crafting and machinery into the game. It will give `humans` ability to craft **items** from ressouces and to combine items with **tools** to make better items. New receipes will be released slowly as the game grow and the cities regroup more and more `humans`.

### Social
**Together!**, yes because it's better. A concept of groups will be implemented. They can share ownership of items and locations, group several `humans` together, take decisions together with ingame interface. It's an alliance.

### New assets
**Create** will give the ability to players to create new designs they will be able to sell to other players. Players can then create a skin for a sword, find a craftman associate and sell the new skined sword to players all around the world. This way, creators can earn XP and guarantee intellectual property.


--------------------------
## Decentralized economy (dEco) - Blockchain role
The blockchain will be used to have a public, immutable state of the economy and a decentralized processing of `humans` skills state, items ownership, crafting processing and land ownership.  

Kapital is the owner of intellectual property of the XP blockchain. In this way, no other company could fork and become the central bank.
If dEarth has to be stopped for any legal reason, the blockchain would fall in the public domain and any company could fork it to be the central bank. 

--------------------------
## XP stability
### Challenge
handling a real economy is hard, you have to take care about inflation/deflation and most importantly: **currency value**.  
`Kapital` role is too keep `XP/fiat` rate stable and to keep ingame price correlated to ingame economy.  
You can see `Kapital` as the central bank of `dEarth`.

### Ingame price
All items are tradables for XP cryptocurrency. All prices ingame will be displayed on local currency (¥, €, $, £). Not so RP but it's a political and ethical choice. We all know that kiddos will play this game one day and I want them to understand it's real money.  
I have 2 little brothers and I want to protect any child as I want to protect them.

### XP stability  
- cryptobubble / cryptokrach
- peak of new `humans` -> monetary mass too small 
- less `humans` or too much gold farmers -> monetary mass too big

The worst consequence would be `XP` price increase which would result in `dEco` costs too much money to run because it relies on smart contracts.  

* When monetary mass is too small, `Kapital` will sell pre mined `XP` at a caped fixed rate and increase loot in basic ressources and `XP`. By fixing a maximum `XP/fiat` rate, `XP` cannot be a speculative crypto.
* When monetary mass is too big, loot rate will be decreased and services price will increase in order to reduce monetary mass.  

By thoses 2 mechanisms, `XP` rate cannot be correlated to bitcoin and thus is protected from cryptobubbles/krachs.


--------------------------
## Blockchain 
### ICO and bonuses
To reward earliest investors whom take the more risks, every step will offer a 100% bonus on the next one: 
1 XPa = 2XPb = 4XPc and so on until we release the blockchain and the XP cryptocurrency.
*dEarth* company will handle the swap with some reserved XPb tokens.  
Tokens will only be used for fund raising and they will be then swaped for **XP** by *dEarth*.  

### dEarth blockchain
In order to be able to process smart contracts with the XP cryptocurrency, the game world needs its own blockchain with XP as gas for smart contracts processing.  
*dEarth* will run all the necessary nodes to handle charge and as blockchain is open sourced, private nodes will appear if gas price is correctly balanced.
 
### pre-mining of XP
XP will BE premined in order to offer loot in game and most important of all: stability.
Premining will allow *dEarth* to hold a virtualy infinite amount of `XP`.

### Token wizard
The ICO has been done thanks to token wizard:  
https://wizard.poa.network/crowdsale?addr=0x2580D616250419cBB435b32173AaFe66e468aCfd&networkID=1


--------------------------
## Technology
### open source
* Game client 
* Api
* Centralized economy of alpha version 
* Blockchain
* State resync between *earth* and the blockchain
* Decentralized economy ( ERC20/ERC1155 on the blockchain )
* Looting algoritm
* wildlife emulation algorithm

### proprietary
* Authoritarive game server ( game state sync client/server ) - interactions humans / earth
* Loot basic items and xp - dispatch xp
* game assets
* services ( marketplace / design creation & patent / skills / betting )
* `humans` management
* map handling

### Community driven
Creation of:
* skills 
* items 
* craft recipes 


--------------------------

## Management
### Staff 
* `Kapital` staff:  
Charged to develop the prorietary technologies, they are hired by `Kapital` company.  
* Contributors:  
Paid as freelances, they are in charge to develop open source technologies with `Kapital` staff. 

### Finances 
The `Kapital` budget will be public and 100% transparent. Salaries will be defined all together to ensure trust and efficiency.

### Keys Productivity Indicators
* euros invested - 150 EUR so far
* hours spent - 85h so far

--------------------------
## Roadmap
There is differents phases for developing the game and decentralizing its economy.  
the alpha stage represents the prototyping of the game: pvp first then open world. Decentralized features will be first developed as centralized while we work on blockchain technology. We want to provide RPG features asap while we need to make them safe and efficient before sealing them in a smart contract. A virtual currency, non-buyable will be used to test this features.

The beta stage will start when we decentralize the economy. A new world (new server) will be opened and we will keep the old one as a traditionnal game server. Think it as "ladder" and "non ladder" in Diablo 2 Battle.net.
 
**Step 1. aug 18 - jan 19 ( 4 months )**  
because we all want to play first, I do a prototype of the pvp game first so we can fight and see who is the best.
In parallel, I prepare the token creation with the community.

**Step 2. jan 19 - jun 19 ( 6 months )**  
Once the fund raised, the team will be hired, be sure I will create a dreamteam of highly skilled professional. You can choose them, I don't want the community to be stuck with non optimal personnal choices.

**Step 3. jan 19 - jan 20**  
Implementation of RPG gameplay in inner app state ( open sourced )
This is a proof of concept and a pre work for writing smart contracts in charge of handling real tokens. Refer to them as pre-contracts, they will be used to write the specs of smart contracts and do strong tests on their behaviour.

Team pre-contract
| priority | name | deadline |
| --- | --- | --- |
| 1. | looting - items | end Q1 19 |
| 2. | pve - xp | end Q2 19 |
| 3. | trading | end Q3 19 |
| 4. | crafting - transformation | end Q4 19 |

Team game
Game client and game server improvement and new features.   


**Step 4. jan 20 - jun 20**  
Integration of pre-contracts into blockchain


--------------------------
## FAQ
**Who are you?**  
I'm an over creative person and this make me able to change an unsolvable problem into a solution, it's so rewarding!
My motto is: 1 hour = 1 new idea.
I'm respectful, I believe in non-violent way of interacting. I strongly believe that humanity stands for peace and that it's just a matter of evolving.
I'm confident, optimistic, I don't like to lie. I'm not perfect, I've done many mistakes in my life but I learnt from every of those and try to be a better human day after day.
I love to see people being successful, I love to teach to children and to raise people up. I strongly believe in the power of the group.
I would prefer to hide my name right now as I'm not sure about the legality of this ICO right now. I'm french and I live in Thaïland.

**What is your professional background ?**  
My dream when I was a child was to make a video game. From 12 to 21 I strongly believed that it was impossible to do it because people told me it was too hard. 
At 21 I understood that people beliefs shouldn't be mine and I started to learn code by myself to make a game.  
I always thought it would be easier to make a traditional business first (service app) to have money to invest in video game industry and that's why I waited so long.
At 29, I'm starting the only game I want to develop since ever, this is my plan for the next 10 years, I'll do it with or without the ICO's success.
My dad gave me the passion for understanding how the financial sector works, which he also learnt by himself. #AutodidactAndEntrepreuneursFamily

**Life goals**  
My main life Goal is to help humanity to build a peaceful and free society. I believe that a real and uncorruptible democracy will give us the ability to leave in sustainable peace.
Internet is a gift to humanity and dEarth is a laboratory for building this kind of tools. 
I also would like to get my pilot license, help my mom to be financially safe till the end of her life, create schools all around the globe where we teach children how to work together, how to be curious and interested by life and how to feel intrinsically safe. I want to be part of the human evolution, I want to be a generator of motivation and ideas. 
I want to invest in hyper-productive farms, respectful of their environment so no human has to suffer from hunger anymore. 
We all know it is possible and we shouldn't be afraid to wish the best for everyone, including us.

**Hobbies**  
I play video games since I can hold a joystick, I do prefer open worlds and freedom than scenario and story. Even if Metal Gear Solid was incredibly well directed !
I love planes and piloting, that's what I do the most when I play GTA but doing it in real life is much more fun.
I really enjoy climbing, swimming, walking on the beach. I enjoy every second spent with my girlfriend. I love to watch sunset and listening to the nature's music.
Coding quickly became a game for me so I guess it should also be in this section ;)


**Why to make an internal virtual game state first before releasing blockchain**
I want this project too work, handling a real world economy is a hard challenge and we must do it step by step to fully understand how to decentralize it while having a central authority, what a paradox! I'm not a blockchain expert, remember that many startup fail because they want to grow too quick. We are commited on a long running project for which I have tons of ideas, and you as the people of `earth` will be part of it.

**what proove me you will work on decentralized economy once you finished to develop it internaly**  
Answer is easy: money. If kapital keeps the economy closed, no one will trust it. Using a crypto currency means people can sell what they earned in game which will leverage interest in the game, which means more money for kapital. I strongly believe in hard currency in video games if economy is real, not correclty balanced but real. 
Without a blockchain, it is nearly impossible for people to trust that we are not manipulating economy. Blockchain will give trust into dEco, trust in dEco will bring money to kapital. This project is much more valuable if it's done entirely. It's all about short term vision vs. long term. This project is my dream for many years (building a RPG over a real economy), I will do it anyway you trust me or not.

**what makes me unable to make another game by myself and use your blockchain as world state ?**
It would be great ! Let's do it and connect our worlds ! We can imagine a 2D web based game living in the same world than a 3D standalone PC game. Why not also a RTS to lead the troups ? The purpose of dEco blockchain is to create an "immutable" world which can be used for anyone who wants to make a pvp game. 

**As your cryptocurrency price will be stable, how can I earn money with it?**
Don't forget that we are making a game relying on blockchain technology and not a high volatility tradable crypto to participate in the bubble. The value of the crypto will go higher as more players comes into the game and more crafting smart contracts are created. dEco is a mirrored copy of our capitalistic real world economy without all the human misery. dEco has a PIB and a value because assets are immutable. It's in our benefit to increase value of the currency over time.

**So we could have different forks for differents game states like different servers ?**
Exactly 

**Is the XPA ICO open ?**
Yes, you can buy tokens here.

**Will you allow raiding or items destruction ?**
This is complicated questions because it means transfering contract ownership without paying for XP. It might be easier to introduce bugs. As `dEarth` run with an authoritarive server and some private data, it will be doable to fork the blockchain in case of big problem. 



## Credits 
wordpress
toggl
https://gitlab.com/l1br3/dEarth
https://discordapp.com/channels/471659163748139028/471659163748139030
https://github.com/poanetwork/token-wizard
https://wordpress.org/plugins/ethereumico
https://www.freepik.com
https://realfavicongenerator.net